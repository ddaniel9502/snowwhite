/* i2c - Example

 For other examples please check:
 https://github.com/espressif/esp-idf/tree/master/examples

 This example code is in the Public Domain (or CC0 licensed, at your option.)

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */
#include "i2c.h"
#include "esp_err.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"

#include "i2c.h"
#include "bno055_cfg.h"
#include "sd_card.h"
#include "gps.h"
#include "TheApp.h"


static const char *TAG = "main";


void Init_modules(void)
{



	ESP_ERROR_CHECK(i2c_master_init());

	if ( 0 == bno055_configuration() )
	{
		ESP_LOGI(TAG, "BNO055 initialization succeeded.\n\r");
	}
	else
	{
		ESP_LOGE(TAG, "BNO055 initialization failed.\n\r");
	}

	//GPS init todo
		GPS_Init();

	/* Check if SdCard is mounted */
	if(CheckSdCard() != 0)
	{

		/* Configure Csv file */
		if( WriteFirstRowInCsv() != 0 )
		{
			ESP_LOGI(TAG, "SD_CARD Initialized\n");
			SdCard_UnMount();
		}
		else
		{
			ESP_LOGE(TAG, "No Data.csv file\n");
		}
	}

	 SD_Card_Read();
}


void app_main()
{
	Init_modules();

	xTaskCreate(task_bno055_normal_mode, "BNO055_task", 1024 * 4, (void* ) 0, 10, NULL);

	xTaskCreate(task_GPS_config, "GPS_main_task", 1024 * 8,(void* ) 0, 10, NULL);

	xTaskCreate(task_Card_main, "Card_main_task", 1024 * 4,(void* ) 0, 10, NULL);
}



