#
# Main Makefile. This is basically the same as a component makefile.
#

CFLAGS += -Dtimegm=mktime

COMPONENT_DIRS += ../Cdd/bno055
COMPONENT_DIRS += ../Cdd/gps
COMPONENT_DIRS += ../Cdd/gps/minmea
COMPONENT_DIRS += ../Cdd/sd_card
COMPONENT_DIRS += ../Periphery/I2c
COMPONENT_DIRS += ../TheApp

COMPONENT_SRCDIRS += ../Cdd/bno055
COMPONENT_SRCDIRS += ../Cdd/gps
COMPONENT_SRCDIRS += ../Cdd/gps/minmea
COMPONENT_SRCDIRS += ../Cdd/sd_card
COMPONENT_SRCDIRS += ../Periphery/I2c
COMPONENT_SRCDIRS += ../TheApp

COMPONENT_ADD_INCLUDEDIRS += ../Cdd/bno055/include
COMPONENT_ADD_INCLUDEDIRS += ../Cdd/gps/include
COMPONENT_ADD_INCLUDEDIRS += ../Cdd/gps/minmea/include
COMPONENT_ADD_INCLUDEDIRS += ../Cdd/sd_card/include
COMPONENT_ADD_INCLUDEDIRS += ../Periphery/I2c/include
COMPONENT_ADD_INCLUDEDIRS += ../TheApp/include
