/*
 * TheApp.c
 *
 *  Created on: Apr 30, 2019
 *      Author: BSSUser
 */

#include "TheApp.h"
#include "esp_err.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "bno055_cfg.h"
//#include "driver/gpio.h"


//static const char *TAG = "SnowWhite";


/*******************read euler converted data*******************/
/* structure used to read the euler hrp data */
struct bno055_euler_double_t d_euler_hrp;

/*********read linear acceleration converted data**********/
/* structure used to read the linear accel xyz data output as m/s2*/
struct bno055_linear_accel_double_t d_linear_accel_xyz;

/*****************read gyro converted data************************/
/* structure used to read the gyro xyz data output as dps or rps */
struct bno055_gyro_double_t d_gyro_xyz;


void task_bno055_normal_mode(void *ignore)
{// STEFAN
//	int blink = 1 ;
//	 gpio_pad_select_gpio(25);
//	    /* Set the GPIO as a push/pull output */
//	    gpio_set_direction(25, GPIO_MODE_OUTPUT);

    /* Variable used to return value of
    communication routine*/
    s32 comres = BNO055_SUCCESS;
    /* variable used to set the power mode of the sensor*/
    u8 power_mode = BNO055_INIT_VALUE;

    /*  For initializing the BNO sensor it is required to the operation mode
        of the sensor as NORMAL
        Normal mode can set from the register
        Page - page0
        register - 0x3E
        bit positions - 0 and 1*/
    power_mode = BNO055_POWER_MODE_NORMAL;
    /* set the power mode as NORMAL*/
    comres += bno055_set_power_mode(power_mode);

    /************************* START READ RAW FUSION DATA ********
                For reading fusion data it is required to set the
                operation modes of the sensor
                operation mode can set from the register
                page - page0
                register - 0x3D
                bit - 0 to 3
                for sensor data read following operation mode have to set
                *FUSION MODE
                    *0x08 - BNO055_OPERATION_MODE_IMUPLUS
                    *0x09 - BNO055_OPERATION_MODE_COMPASS
                    *0x0A - BNO055_OPERATION_MODE_M4G
                    *0x0B - BNO055_OPERATION_MODE_NDOF_FMC_OFF
                    *0x0C - BNO055_OPERATION_MODE_NDOF
                    based on the user need configure the operation mode*/
    comres += bno055_set_operation_mode(BNO055_OPERATION_MODE_NDOF);

    while (1) {
    	//Stefan
    	      //  printf("START SENZOR TASK\n ");

//    	blink=!blink;
//    			gpio_set_level(25, blink);
        /******************START READ CONVERTED SENSOR DATA****************/
        /*  API used to read Euler data output as double  - degree and radians
            float functions also available in the BNO055 API */
        comres += bno055_convert_double_euler_hpr_deg(&d_euler_hrp);

//        if ( !comres )
//        {
//            ESP_LOGI(TAG, "ED_h: %.3f, ED_r: %.3f, ED_p: %.3f; [d]", d_euler_hrp.h, d_euler_hrp.r, d_euler_hrp.p);
//        }

        /*  API used to read Linear acceleration data output as m/s2
            float functions also available in the BNO055 API */
        comres += bno055_convert_double_linear_accel_xyz_msq(
        &d_linear_accel_xyz);

//        if ( !comres )
//        {
//            ESP_LOGI(TAG, "LA_x: %.3f, LA_y: %.3f, LA_z: %.3f; [m/s2]", d_linear_accel_xyz.x, d_linear_accel_xyz.y, d_linear_accel_xyz.z);
//        }

        /*  API used to read gyro data output as double  - dps
            float functions also available in the BNO055 API */
        comres += bno055_convert_double_gyro_xyz_dps(&d_gyro_xyz);

//        if ( !comres )
//        {
//            ESP_LOGI(TAG, "GY_x: %.3f, GY_y: %.3f, GY_z: %.3f; [dps]", d_gyro_xyz.x, d_gyro_xyz.y, d_gyro_xyz.z);
//        }

        /* Task delay */
        vTaskDelay(10 / portTICK_PERIOD_MS);
       // printf("STOP  SENZOR TASK\n ");
    }
    vTaskDelete(NULL);
}


double getLinearAcceleration_x(void)
{
    return d_linear_accel_xyz.x;
}


double getLinearAcceleration_y(void)
{
    return d_linear_accel_xyz.y;
}


double getLinearAcceleration_z(void)
{
    return d_linear_accel_xyz.z;
}


double getEulerAngle_h(void)
{
    return d_euler_hrp.h;
}


double getEulerAngle_r(void)
{
    return d_euler_hrp.r;
}


double getEulerAngle_p(void)
{
    return d_euler_hrp.p;
}


double getGyro_x(void)
{
    return d_gyro_xyz.x;
}


double getGyro_y(void)
{
    return d_gyro_xyz.y;
}


double getGyro_z(void)
{
    return d_gyro_xyz.z;
}

