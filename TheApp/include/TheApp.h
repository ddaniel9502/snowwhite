/*
 * TheApp.h
 *
 *  Created on: Apr 30, 2019
 *      Author: BSSUser
 */

#ifndef THEAPP_THEAPP_H_
#define THEAPP_THEAPP_H_


void task_bno055_normal_mode(void *ignore);


double getLinearAcceleration_x(void);
double getLinearAcceleration_y(void);
double getLinearAcceleration_z(void);

double getEulerAngle_h(void);
double getEulerAngle_r(void);
double getEulerAngle_p(void);

double getGyro_x(void);
double getGyro_y(void);
double getGyro_z(void);


#endif /* THEAPP_THEAPP_H_ */
