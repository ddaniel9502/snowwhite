/*
 * gps.h
 *
 *  Created on: Apr 24, 2019
 *      Author: BSS
 */
// FreeRTOS includes
 #include "freertos/FreeRTOS.h"
 #include "freertos/task.h"

// UART driver
 #include "driver/uart.h"
 #include "driver/gpio.h"

//#include "sd_card.h"

#ifndef COMPONENTS_MINMEA_GPS_H_
#define COMPONENTS_MINMEA_GPS_H_

typedef struct gps_time_date_type {
    int hours;
    int minutes;
    int seconds;
    int miliseconds;
    int day;
    int month;
    int year;
}gps_time_date_t;

typedef struct gps_save_data_type{
	struct gps_time_date_type time_date;
	float latitude;
    float longitude;
    float speed_kmh;
    int NewData;
}gps_save_data_t;
 gps_save_data_t gps_save_data_s;


void GPS_Init();
char* read_line(uart_port_t uart_controller);
void task_GPS_config(void *ignore);

int getTimeDateHoursFromGps(void);
int getTimeDateMinutesFromGps(void);
int getTimeDateSecondsFromGps(void);
int getTimeDateMiliSecondsFromGps(void);
int getTimeDateDayFromGps(void);
int getTimeDateMonthFromGps(void);
int getTimeDateYearFromGps(void);
float getLatitudeFromGps(void);
//float getLatitudeFromGps(void);
float getLongitudeFromGps(void);
float getSpeedKmhFromGps(void);
int getNewDataFromGps(void);


#endif /* COMPONENTS_MINMEA_GPS_H_ */
