// minmea
#include "minmea.h"
#include "gps.h"
//#include "sd_card.h"

// Error library
//#include "esp_err.h"

#define GPS_UART_NUM 	UART_NUM_2
#define UART_BUF_SIZE	(512)
#define TX_GPIO 		GPIO_NUM_17
#define RX_GPIO 		GPIO_NUM_16

#define BLINK_GPIO 26
static QueueHandle_t 	uart0_queue;

static float latitude;
static float longitude;
float altitude;
float height;
static float speed;
int fix_quality;
int satellites_tracked;

void GPS_Init()
{
	gpio_pad_select_gpio(BLINK_GPIO);
	/* Set the GPIO as a push/pull output */
	gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);


	// configure the UART2 controller, connected to the GPS receiver
	uart_config_t uart_config = {
			  //.baud_rate = 115200,
			 .baud_rate = 9600,
			.data_bits = UART_DATA_8_BITS,
			.parity    = UART_PARITY_DISABLE,
			.stop_bits = UART_STOP_BITS_1,
			.flow_ctrl = UART_HW_FLOWCTRL_DISABLE
	};
	uart_param_config(GPS_UART_NUM, &uart_config);
	uart_set_pin(GPS_UART_NUM, TX_GPIO, RX_GPIO, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
	//uart_driver_install(GPS_UART_NUM, 1024, 0, 0, NULL, 0);
	uart_driver_install(GPS_UART_NUM, UART_BUF_SIZE * 2, UART_BUF_SIZE * 2, 20, uart0_queue, 0);

	// GPS variables and initial state
	latitude = -1.0;
	longitude = -1.0;
	//altitude = 0;
	//height = 0;
	speed = 0;
	//fix_quality = -1;
	satellites_tracked = -1;
	gps_save_data_s.latitude = 0;
	gps_save_data_s.longitude = 0;
	gps_save_data_s.speed_kmh = 0;
	gps_save_data_s.NewData = 0U;
}

// read a line from the UART controller
char* read_line(uart_port_t uart_controller) {

	static char line[MINMEA_MAX_LENGTH];
	char *ptr = line;
	while(1) {

		int num_read = uart_read_bytes(uart_controller, (unsigned char *)ptr, 1, portMAX_DELAY);
		if(num_read == 1) {

			// new line found, terminate the string and return
			if(*ptr == '\n') {
				ptr++;
				*ptr = '\0';
				return line;
			}

			// else move to the next char
			ptr++;
		}
	}
}

// Main application
void task_GPS_config(void *ignore)
{
	//int blink = 1 ;
	while(1) {

		//blink=!blink;
		//gpio_set_level(26, blink);
		/*if (blink == 1 )
		{//Turning on the LED
			gpio_set_level(26, !(blink));
			blink=0;
		}else{
			//Turning of the LED
			gpio_set_level(26, blink);
			blink=1;
		}*/


		//printf("START  GPS TASK\n ");
		gps_save_data_s.NewData=0U;
		// read a line from the receiver
		char *line =  read_line(GPS_UART_NUM);
		switch (minmea_sentence_id(line, false)) {

		case MINMEA_SENTENCE_RMC: {

			struct minmea_sentence_rmc frame;
			if (minmea_parse_rmc(&frame, line)) {

				gps_save_data_s.NewData=0;
				// latitude valid and changed? apply a threshold
				float new_latitude = minmea_tocoord(&frame.latitude);
				if((new_latitude != NAN) && (abs(new_latitude - latitude) > 0.0001)) {
					latitude = new_latitude;
					printf("New latitude: %f\n", latitude);
				}
				if(new_latitude == NAN)
				{
					new_latitude = 0U;
				}
				latitude = new_latitude;

				// longitude valid and changed? apply a threshold
				float new_longitude = minmea_tocoord(&frame.longitude);
				if((new_longitude != NAN) && (abs(new_longitude - longitude) > 0.0001)) {
					longitude = new_longitude;
					printf("New longitude: %f\n", longitude);
				}
				if(new_longitude == NAN)
				{
					new_longitude = 0U;
				}
				longitude = new_longitude;

				//Speed
				float new_speed = minmea_tocoord(&frame.speed);
				if(speed != new_speed)
				{
					gps_save_data_s.NewData=1U;

				}
				else{
					gps_save_data_s.NewData=0U;
				}

				if(new_speed == NAN)
				{
					new_speed = 0U;
				}
				else
				{
					speed = new_speed;
				}

				//Save data
				if(frame.time.hours != -1)
					gps_save_data_s.time_date.hours = frame.time.hours+2;
				if(frame.time.minutes != -1)
					gps_save_data_s.time_date.minutes = frame.time.minutes;
				if(frame.time.seconds != -1)
					gps_save_data_s.time_date.seconds = frame.time.seconds;
				if(frame.time.microseconds != -1)
					gps_save_data_s.time_date.miliseconds = ((frame.time.microseconds)*1000);
				if(frame.date.day != -1)
					gps_save_data_s.time_date.day = frame.date.day;
				if(frame.date.month != -1)
					gps_save_data_s.time_date.month = frame.date.month;
				if(frame.date.year != -1)
					gps_save_data_s.time_date.year = frame.date.year;

				if(latitude != NAN)
				{gps_save_data_s.latitude  = latitude;}
				if(longitude != NAN)
				{gps_save_data_s.longitude  = longitude;}
				if((speed!= NAN) && (speed!=0x00))
				{gps_save_data_s.speed_kmh  = speed;}


				/*printf("\nNew time: %d:%02d:%02d:%02d UTC %02d.%02d.%d\n",\
						frame.time.hours+3,\
						frame.time.minutes,\
						frame.time.seconds,\
						frame.time.microseconds*3,\
						frame.date.day,\
						frame.date.month,\
						frame.date.year);*/
			}
		} break;


		case MINMEA_SENTENCE_GGA: {

			struct minmea_sentence_gga frame;
			if (minmea_parse_gga(&frame, line)) {
				// fix quality changed?
				if(frame.fix_quality != fix_quality) {
					fix_quality = frame.fix_quality;
					printf("New fix quality: %d\n", fix_quality);
				}

				// latitude valid and changed? apply a threshold
				float new_altitude = minmea_tocoord(&frame.altitude);
				//	if((new_latitude != NAN) && (abs(new_latitude - latitude) > 0.001)) {
				altitude = new_altitude;
				printf("New altitude: %f %c\n", altitude, frame.altitude_units);
				//	}
				// height valid and changed? apply a threshold
				float new_height = minmea_tocoord(&frame.height);
				//	if((new_latitude != NAN) && (abs(new_latitude - latitude) > 0.001)) {
				height = new_height;
				printf("New height: %f %c\n", height, frame.height_units);
				//	}


			}
		} break;

		case MINMEA_SENTENCE_GSV: {

			struct minmea_sentence_gsv frame;
			if (minmea_parse_gsv(&frame, line)) {
				// number of satellites changed?
				if(frame.total_sats != satellites_tracked) {
					satellites_tracked = frame.total_sats;
					printf("New satellites tracked: %d\n", satellites_tracked);
				}
			}
		} break;

		case MINMEA_SENTENCE_VTG: {

			struct minmea_sentence_vtg frame;
			if (minmea_parse_vtg(&frame, line)) {
				// number of satellites changed?
				//if(frame.total_sats != satellites_tracked) {
				//satellites_tracked = frame.total_sats;
				float new_speed = minmea_tocoord(&frame.speed_kph);
				speed = new_speed;
				printf("New speed: %f kph\n", speed);
				//}
			}
		} break;

		default: break;
		}
		vTaskDelay(120 / portTICK_PERIOD_MS);
		//printf("STOP  GPS TASK\n ");
		//gpio_set_level(26, 0);
	}
	vTaskDelete(NULL);
}


int getTimeDateHoursFromGps(void)
{
	return gps_save_data_s.time_date.hours;
}

int getTimeDateMinutesFromGps(void)
{
	return gps_save_data_s.time_date.minutes;
}

int getTimeDateSecondsFromGps(void)
{
	return gps_save_data_s.time_date.seconds;
}
int getTimeDateMiliSecondsFromGps(void)
{
	return gps_save_data_s.time_date.miliseconds;
}

int getTimeDateDayFromGps(void)
{
	return gps_save_data_s.time_date.day;
}

int getTimeDateMonthFromGps(void)
{
	return gps_save_data_s.time_date.month;
}

int getTimeDateYearFromGps(void)
{
	return gps_save_data_s.time_date.year;
}

float getLatitudeFromGps(void)
{
	return gps_save_data_s.latitude;
}

float getLongitudeFromGps(void)
{
	return gps_save_data_s.longitude;
}

float getSpeedKmhFromGps(void)
{
	return gps_save_data_s.speed_kmh;
}

int getNewDataFromGps(void)
{
	return gps_save_data_s.NewData;
}
