/* SD card and FAT filesystem example.
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
 */

#include "sd_card.h"
#include "TheApp.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include "esp_err.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "freertos/task.h"
#include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"
//#include "sd_card.h"
//#include "TheApp.h"

//static const char *TAG = "example";

static float ThresholdSpeed = 0;
int Old_Speed = 0;

/*	STRUCTURE DEFINITIONS	*/
//static struct SD_bno055_t *p_bno055;

static const char *TAG = "SD_CARD";

// This example can use SDMMC and SPI peripherals to communicate with SD card.
// By default, SDMMC peripheral is used.
// To enable SPI mode, uncomment the following line:

#define USE_SPI_MODE


// When testing SD and SPI modes, keep in mind that once the card has been
// initialized in SPI mode, it can not be reinitialized in SD mode without
// toggling power to the card.

#ifdef USE_SPI_MODE
// Pin mapping when using SPI mode.
// With this mapping, SD card can be used both in SPI and 1-line SD mode.
// Note that a pull-up on CS line is required in SD mode.
#define PIN_NUM_MISO 19
#define PIN_NUM_MOSI 23
#define PIN_NUM_CLK  18
#define PIN_NUM_CS   5
#endif //USE_SPI_MODE

#define BLINK_GPIO 27

// 2 Structure for BNO055
//struct SD_bno055_t bno055Data;
//struct SD_bno055_t p_bno055;
FILE* fileToWrite;
FILE* fileToRead;
u32 lenSizeOfWrite;

/*void reverse(char *str, int len)
{
    int i=0, j=len-1, temp;
    while (i<j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}

 // Converts a given integer x to string str[].  d is the number
 // of digits required in output. If d is more than the number
 // of digits in x, then 0s are added at the beginning.
int intToStr(int x, char str[], int d)
{
    int i = 0;
    while (x)
    {
        str[i++] = (x%10) + '0';
        x = x/10;
    }

    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';

    reverse(str, i);
    str[i] = '\0';
    return i;
}

void ftoa(float n, char *res, int afterpoint)
{
    // Extract integer part
    int ipart = (int)n;

    // Extract floating part
    float fpart = n - (float)ipart;

    // convert integer part to string
    int i = intToStr(ipart, res, 0);

    // check for display option after point
    if (afterpoint != 0)
    {
        res[i] = '.';  // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);

        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}*/

int n_tu(int number, int count)
{
	int result = 1;
	while(count-- > 0)
		result *= number;

	return result;
}

/*** Convert float to string ***/
void float_to_string(float f, char r[])
{
	long long int length, length2, i, number, position, sign;
	float number2;

	sign = -1;   // -1 == positive number
	if (f < 0)
	{
		sign = '-';
		f *= -1;
	}

	number2 = f;
	number = f;
	length = 0;  // Size of decimal part
	length2 = 0; // Size of tenth

	/* Calculate length2 tenth part */
	while( (number2 - (float)number) != 0.0 && !((number2 - (float)number) < 0.0) )
	{
		number2 = f * (n_tu(10.0, length2 + 1));
		number = number2;

		length2++;
	}

	/* Calculate length decimal part */
	for (length = (f > 1) ? 0 : 1; f > 1; length++)
		f /= 10;

	position = length;
	length = length + 1 + length2;
	number = number2;
	if (sign == '-')
	{
		length++;
		position++;
	}

	for (i = length; i >= 0 ; i--)
	{
		if (i == (length))
			r[i] = '\0';
		else if(i == (position))
			r[i] = '.';
		else if(sign == '-' && i == 0)
			r[i] = '-';
		else
		{
			r[i] = (number % 10) + '0';
			number /=10;
		}
	}
}


u8 CheckSdCard(void)
{

	ESP_LOGI(TAG, "Check SD Card is mounted");
	u8 SdCardFlag = 1;
	sdmmc_host_t host = SDSPI_HOST_DEFAULT();
	host.max_freq_khz =SDMMC_FREQ_HIGHSPEED;

	sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
	slot_config.gpio_miso = PIN_NUM_MISO;
	slot_config.gpio_mosi = PIN_NUM_MOSI;
	slot_config.gpio_sck  = PIN_NUM_CLK;
	slot_config.gpio_cs   = PIN_NUM_CS;

	sdspi_host_set_card_clk( host.max_freq_khz,SDMMC_FREQ_HIGHSPEED);

	// Options for mounting the filesystem.
	// If format_if_mount_failed is set to true, SD card will be partitioned and
	// formatted in case when mounting fails.
	esp_vfs_fat_sdmmc_mount_config_t mount_config = {
			.format_if_mount_failed = false,
			.max_files = 5,
			.allocation_unit_size = 16 * 1024
	};

	// Use settings defined above to initialize SD card and mount FAT filesystem.
	// Note: esp_vfs_fat_sdmmc_mount is an all-in-one convenience function.
	// Please check its source code and implement error recovery when developing
	// production applications.
	sdmmc_card_t* card;
	esp_err_t ret = esp_vfs_fat_sdmmc_mount("/scan", &host, &slot_config, &mount_config, &card);

	if (ret != ESP_OK) {
		if (ret == ESP_FAIL) {
			ESP_LOGE(TAG, "Failed to mount filesystem. "
					"If you want the card to be formatted, set format_if_mount_failed = true.");
		} else {
			ESP_LOGE(TAG, "Failed to initialize the card (%s). "
					"Make sure SD card lines have pull-up resistors in place.", esp_err_to_name(ret));
		}
		SdCardFlag = 0;
	}

	// Card has been initialized, print its properties
	//sdmmc_card_print_info(stdout, card);
	return SdCardFlag;
}

u8 WriteFirstRowInCsv(void)
{
	u8 flag = 1;

	ESP_LOGI(TAG, "Opening file");
	fileToWrite = fopen("/scan/Data.csv", "w");
	if (fileToWrite == NULL) {
		ESP_LOGE(TAG, "Failed to open file for writing");
		flag = 0;
	}

	//char str [] = "TIME, LATITUDE, LONGITUDE, SPEED, XAXEL, YAXEL, ZAXEL, XEULER, YEULER, ZEULER, XGIRO, YGIRO, ZGIRO, TEMP \n";
	char str [] = "C, TIME, SPEED, LATITUDE, LONGITUDE,  XAXEL, YAXEL, ZAXEL, XEULER, YEULER, ZEULER, XGIRO, YGIRO, ZGIRO, \n";

	fwrite(str , 1, sizeof(str) , fileToWrite);
	fclose(fileToWrite);
	return flag;
}

void SdCard_UnMount(void)
{
	esp_vfs_fat_sdmmc_unmount();
	ESP_LOGI(TAG, "Card unmounted");
}

// TASK
void task_Card_main(void *ignore)
{


	gpio_pad_select_gpio(BLINK_GPIO);
	gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);


	//int blink = 1 ;

	while(1)
	{
		float speed_get = 0;
		//int statusofdata = 0;
		//get speed form gps
		speed_get = getSpeedKmhFromGps();

		//		if (Old_Speed != speed_get)
		//		{
		//			statusofdata = 1U;
		//			Old_Speed = speed_get;
		//		}
		//		else
		//		{
		//			statusofdata = 0U;
		//		}

		// SPEED ABOVE A SET VALUE
		if(speed_get <  ThresholdSpeed)
		{
			// not logging
			/* Blink off (output low) */
			printf("Turning off the LED\n");
			gpio_set_level(BLINK_GPIO, 0);
			/* Task delay */
			vTaskDelay(100 / portTICK_PERIOD_MS);

		}
		else
		{

			//start to write
			if(CheckSdCard() != 0)
			{
				fileToWrite = fopen("/scan/Data.csv", "a+");
				if (fileToWrite == NULL) {
					ESP_LOGE(TAG, "Failed to open file for writing");

					gpio_set_level(BLINK_GPIO, 0);
					/* Task delay */
					vTaskDelay(100 / portTICK_PERIOD_MS);


				}
				else
				{

					int dataInt;
					float dataFloat=0;
					double dataDouble;
					char snum[64];
					memset(snum,0,64);

					//Turning on the LED
					printf("Turning on the LED\n");
					gpio_set_level(BLINK_GPIO, 1);

					/* GPS DATA */
					ESP_LOGI(TAG, "Write GPS Data to SDCard");

					dataInt = getNewDataFromGps();
					printf("Status: %d\n",dataInt);
					itoa(dataInt, snum, 10);
					//printf("StatusnString: %s\n",snum);
					fwrite(snum, 1, sizeof(int), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);

					dataInt = getTimeDateHoursFromGps();
					printf("Hours: %d\n",dataInt);
					itoa(dataInt, snum, 10);
					printf("HoursInString: %s\n",snum);
					fwrite( snum, 1, sizeof(int), fileToWrite);
					fwrite( ":", 1, 2, fileToWrite);

					dataInt = getTimeDateMinutesFromGps();
					//printf("Minutes: %d\n",dataInt);
					itoa(dataInt, snum, 10);
					//printf("MinutesInString: %s\n",snum);
					fwrite( snum, 1, sizeof(int), fileToWrite);
					fwrite( ":", 1, 2, fileToWrite);

					dataInt = getTimeDateSecondsFromGps();
					//printf("Second: %d\n",dataInt);
					itoa(dataInt, snum, 10);
					printf("SecondInString: %s\n",snum);
					fwrite( snum, 1, sizeof(int), fileToWrite);
					fwrite( ":", 1, 2, fileToWrite);

					dataInt = getTimeDateMiliSecondsFromGps();
					//printf("MiliSecond: %d\n",dataInt);
					itoa(dataInt, snum, 10);
					//printf("MiliSecondInString: %s\n",snum);
					fwrite( snum, 1, sizeof(int), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);;


					//Speed
					dataFloat = getSpeedKmhFromGps();
					printf("Speed: %f\n",dataFloat);
					if(dataFloat == NAN)
					{dataFloat= 0U;}
					//conversion
					gcvt(dataFloat, 8U, snum);
					printf("SpeedInString: %s\n",snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);


					//Latitude
					dataFloat = getLatitudeFromGps();
					printf("Latitude: %f\n",dataFloat);
					printf("Latitude: is a not a number!\n");
					if(dataFloat == NAN)
					{dataFloat= 0U;}
					//conversion
					gcvt(dataFloat, 8U, snum);
					printf("LatitudeInString: %s\n",snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);

					//Longitude
					dataFloat = getLongitudeFromGps();
					printf("Longitude: %f\n",dataFloat);
					if(dataFloat == NAN)
					{dataFloat= 0U;}
					//conversion
					gcvt(dataFloat, 8U, snum);
					printf("LongitudeInString: %s\n",snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);


					//LINEAR ACCELERATION
					dataDouble = getLinearAcceleration_x();
					//ESP_LOGI(TAG, "LinearAcceleration_x: %.3f", dataDouble);
					//conversion
					float_to_string(dataDouble,snum);
					//ESP_LOGI(TAG, "LinearAcceleration_x in String: %s\n", snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);

					dataDouble = getLinearAcceleration_y();
					//ESP_LOGI(TAG, "LinearAcceleration_y: %.3f", dataDouble);
					//conversion
					float_to_string(dataDouble,snum);
					//ESP_LOGI(TAG, "LinearAcceleration_y in String: %s\n", snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);

					dataDouble = getLinearAcceleration_z();
					//ESP_LOGI(TAG, "LinearAcceleration_z: %.3f", dataDouble);
					//conversion
					float_to_string(dataDouble,snum);
					//ESP_LOGI(TAG, "LinearAcceleration_z in String: %s\n", snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);


					//EULER ANGLE
					dataDouble = getEulerAngle_h();
					//ESP_LOGI(TAG, "EulerAngle_h: %.3f", dataDouble);
					//conversion
					float_to_string(dataDouble,snum);
					//ESP_LOGI(TAG, "EulerAngle_h in String: %s\n", snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);

					dataDouble = getEulerAngle_r();
					//ESP_LOGI(TAG, "EulerAngle_r: %.3f", dataDouble);
					//conversion
					float_to_string(dataDouble,snum);
					//ESP_LOGI(TAG, "EulerAngle_r in String: %s\n", snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);

					dataDouble = getEulerAngle_p();
					//ESP_LOGI(TAG, "EulerAngle_p: %.3f", dataDouble);
					//conversion
					float_to_string(dataDouble,snum);
					//ESP_LOGI(TAG, "EulerAngle_p in String: %s\n", snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);


					//ANGULAR RATE
					dataDouble = getGyro_x();
					//ESP_LOGI(TAG, "Gyro_x: %.3f", dataDouble);
					//conversion
					float_to_string(dataDouble,snum);
					//ESP_LOGI(TAG, "Gyro_x in String: %s\n", snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);

					dataDouble = getGyro_y();
					//conversion
					float_to_string(dataDouble,snum);
					//ESP_LOGI(TAG, "Gyro_y in String: %s\n", snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( ", ", 1, 2, fileToWrite);

					dataDouble = getGyro_z();
					//ESP_LOGI(TAG, "Gyro_z: %.3f", dataDouble);
					//conversion
					float_to_string(dataDouble,snum);
					//ESP_LOGI(TAG, "Gyro_z in String: %s\n", snum);
					fwrite( snum, 1, sizeof(double), fileToWrite);
					fwrite( " \n", 1, 2, fileToWrite);


					fclose(fileToWrite);
					SdCard_UnMount();
				}
			}
			else
			{
				gpio_set_level(BLINK_GPIO, 0);
				/* Task delay */
				vTaskDelay(100 / portTICK_PERIOD_MS);

			}
		}
		/* Task delay */
		vTaskDelay(2 / portTICK_PERIOD_MS);
	}
	vTaskDelete(NULL);
}


void SD_Card_Read(void)
{

	if(CheckSdCard() != 0)
	{
		fileToRead =  fopen("/scan/Conf.txt", "r");
		if (fileToRead == NULL) {
			ESP_LOGE(TAG, "Failed to open file for reading");
			//return;

			ThresholdSpeed= 0;
			printf("Speed: %f\n",ThresholdSpeed);

			esp_vfs_fat_sdmmc_unmount();
			ESP_LOGI(TAG, "Card unmounted");
		}
		else
		{
			//Read data
			char line[64];
			fgets(line, sizeof(line), fileToRead);
			fclose(fileToRead);
			// strip newline
			char* pos = strchr(line, '\n');
			if (pos) {
				*pos = '\0';
			}
			ESP_LOGI(TAG, "Read from file: '%s'", line);

			ThresholdSpeed = atof(line);

			printf("Speed: %f\n",ThresholdSpeed);

			esp_vfs_fat_sdmmc_unmount();
			ESP_LOGI(TAG, "Card unmounted");
		}
	}
}

