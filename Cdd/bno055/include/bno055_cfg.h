/*
 * bno055_cfg.h
 *
 *  Created on: Apr 26, 2019
 *      Author: BSSUser
 */

#ifndef CDD_BNO055_INCLUDE_BNO055_CFG_H_
#define CDD_BNO055_INCLUDE_BNO055_CFG_H_


#include "bno055.h"


s32 bno055_configuration(void);


#endif /* CDD_BNO055_INCLUDE_BNO055_CFG_H_ */
