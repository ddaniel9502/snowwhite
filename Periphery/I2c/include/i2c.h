/*
 * i2c.h
 *
 *  Created on: Apr 26, 2019
 *      Author: BSSUser
 */

#ifndef PERIPHERY_I2C_INCLUDE_I2C_H_
#define PERIPHERY_I2C_INCLUDE_I2C_H_

#include <stdint.h>
#include "esp_err.h"

esp_err_t i2c_master_init(void);
uint8_t I2C_bus_write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);
uint8_t I2C_bus_read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt);


#endif /* PERIPHERY_I2C_INCLUDE_I2C_H_ */
