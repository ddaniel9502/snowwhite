/*
 * i2c.c
 *
 *  Created on: Apr 26, 2019
 *      Author: BSSUser
 */

#include "i2c.h"

#include "esp_err.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/i2c.h"

#define SDA_PIN GPIO_NUM_21
#define SCL_PIN GPIO_NUM_22

#define I2C_ACK_CHECK_EN    0x01
#define I2C_ACK_CHECK_DIS   0x00
#define I2C_MASTER_ACK      0x00
#define I2C_MASTER_NACK     0x01

#define I2C_MASTER_RX_BUF_DISABLE 0
#define I2C_MASTER_TX_BUF_DISABLE 0


esp_err_t i2c_master_init(void)
{
	esp_err_t errl_result;

	i2c_config_t i2c_config = {
			.mode = I2C_MODE_MASTER,
			.sda_io_num = SDA_PIN,
			.scl_io_num = SCL_PIN,
			.sda_pullup_en = GPIO_PULLUP_ENABLE,
			.scl_pullup_en = GPIO_PULLUP_ENABLE,
			.master.clk_speed = 400000
	};

	errl_result = i2c_param_config(I2C_NUM_0, &i2c_config);
	ESP_ERROR_CHECK_WITHOUT_ABORT(errl_result);

	errl_result = i2c_driver_install(I2C_NUM_0, i2c_config.mode,
			I2C_MASTER_RX_BUF_DISABLE,
			I2C_MASTER_TX_BUF_DISABLE, 0);
	ESP_ERROR_CHECK_WITHOUT_ABORT(errl_result);

	/* Set timeout to 1ms. Default is 6400 cycles.
	 * 1 cycle equals to I2C bus ABP speed - 80MHz*/
	i2c_set_timeout(I2C_NUM_0, 80000);

	return errl_result;
}


/*  \Brief: The API is used as I2C bus write
 *  \Return : Status of the I2C write
 *  \param dev_addr : The device address of the sensor
 *  \param reg_addr : Address of the first register,
 *   will data is going to be written
 *  \param reg_data : It is a value hold in the array,
 *      will be used for write the value into the register
 *  \param cnt : The no of byte of data to be write
 */
uint8_t I2C_bus_write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt)
{
	uint8_t iError = EXIT_SUCCESS;
	esp_err_t espRc;
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();

	espRc = i2c_master_start(cmd);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	espRc = i2c_master_write_byte(cmd, (dev_addr << 1) | I2C_MASTER_WRITE, I2C_ACK_CHECK_EN);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	espRc = i2c_master_write_byte(cmd, reg_addr, I2C_ACK_CHECK_EN);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	espRc = i2c_master_write(cmd, reg_data, cnt, I2C_ACK_CHECK_EN);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	espRc = i2c_master_stop(cmd);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);

	espRc = i2c_master_cmd_begin(I2C_NUM_0, cmd, 10 / portTICK_PERIOD_MS);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	i2c_cmd_link_delete(cmd);

	if (espRc == ESP_OK) {
		iError = EXIT_SUCCESS;
	} else {
		iError = EXIT_FAILURE;
	}

	return iError;
}


/*  \Brief: The API is used as I2C bus read
 *  \Return : Status of the I2C read
 *  \param dev_addr : The device address of the sensor
 *  \param reg_addr : Address of the first register,
 *  will data is going to be read
 *  \param reg_data : This data read from the sensor,
 *   which is hold in an array
 *  \param cnt : The no of byte of data to be read
 */
uint8_t I2C_bus_read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data, uint8_t cnt)
{
	uint8_t iError = EXIT_SUCCESS;
	esp_err_t espRc;
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();

	espRc = i2c_master_start(cmd);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	espRc = i2c_master_write_byte(cmd, (dev_addr << 1) | I2C_MASTER_WRITE, I2C_ACK_CHECK_EN);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	espRc = i2c_master_write_byte(cmd, reg_addr, I2C_ACK_CHECK_EN);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);

	espRc = i2c_master_start(cmd);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	espRc = i2c_master_write_byte(cmd, (dev_addr << 1) | I2C_MASTER_READ, I2C_ACK_CHECK_EN);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);

	if (cnt > 1) {
		espRc = i2c_master_read(cmd, reg_data, cnt - 1, I2C_MASTER_ACK);
		ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	}
	espRc = i2c_master_read_byte(cmd, reg_data + cnt - 1, I2C_MASTER_NACK);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	espRc = i2c_master_stop(cmd);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);

	espRc = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_PERIOD_MS);
	ESP_ERROR_CHECK_WITHOUT_ABORT(espRc);
	i2c_cmd_link_delete(cmd);

	if (espRc == ESP_OK) {
		iError = EXIT_SUCCESS;
	} else {
		iError = EXIT_FAILURE;
	}

	return iError;
}
